

#include "XtremeMonopoly.h"
#include "XtremeMonopolyPlayerController.h"
#include "MyPlayerState.h"
#include "XtremeMonopolyGameMode.h"

//AXtremeMonopolyPlayerController::AXtremeMonopolyPlayerController(const class FPostConstructInitializeProperties& PCIP)
//	: Super(PCIP)
//{
//
//}
//
//

void AXtremeMonopolyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if (Role == ROLE_Authority)
	{
		GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::MakeRandomColor(), TEXT("I'M THE AUTHORITY!"));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::MakeRandomColor(), TEXT("I'M NOT THE AUTHORITY!"));
	}*/
}

void AXtremeMonopolyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("WinTest", IE_Pressed, this, &AXtremeMonopolyPlayerController::OnWinTrigger);

	/*
	// UI input
	InputComponent->BindAction("InGameMenu", IE_Pressed, this, &AShooterPlayerController::OnToggleInGameMenu);
	InputComponent->BindAction("Scoreboard", IE_Pressed, this, &AShooterPlayerController::OnShowScoreboard);
	InputComponent->BindAction("Scoreboard", IE_Released, this, &AShooterPlayerController::OnHideScoreboard);
	InputComponent->BindAction("ConditionalCloseScoreboard", IE_Pressed, this, &AShooterPlayerController::OnConditionalCloseScoreboard);
	InputComponent->BindAction("ToggleScoreboard", IE_Pressed, this, &AShooterPlayerController::OnToggleScoreboard);

	// voice chat
	InputComponent->BindAction("PushToTalk", IE_Pressed, this, &APlayerController::StartTalking);
	InputComponent->BindAction("PushToTalk", IE_Released, this, &APlayerController::StopTalking);

	InputComponent->BindAction("ToggleChat", IE_Pressed, this, &AShooterPlayerController::ToggleChatWindow);
	*/
}

void AXtremeMonopolyPlayerController::OnWinTrigger()
{
	AXtremeMonopolyGameMode* const GameMode = Cast<AXtremeMonopolyGameMode>(GetWorld()->GetAuthGameMode());

	if (GameMode)
		GameMode->CheckForWin();

}
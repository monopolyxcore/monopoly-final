// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "XtremeMonopolyHUD.h"
#include "GameMenu.h"

AXtremeMonopolyHUD::AXtremeMonopolyHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	struct FontStatic //define fonts here
	{
		ConstructorHelpers::FObjectFinder<UFont> Object0;
		FontStatic()
			: Object0(TEXT("/Engine/EngineFonts/RobotoDistanceField"))
		{
		}		
	};
	static FontStatic fontObject;
	
	struct TextureStatic //define textures here
	{
		ConstructorHelpers::FObjectFinder<UTexture> Object0;
		TextureStatic()
			: Object0(TEXT("/Game/RoundedSquare"))
		{
		}

	};
	static TextureStatic textureObject;
	
	HUDFont = fontObject.Object0.Object;
	
	Icon1 = textureObject.Object0.Object;
}

void AXtremeMonopolyHUD::DrawHUD()
{
	ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);
	Super::DrawHUD();
	GameMenu menu = GameMenu(this);
	menu.OnChangeState(1);
	menu.Update();
	//DrawTexture(Icon1, 100, 100, 100, 100, 0, 0, 1, 1);
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UIElement.h"
class UIElement;

/**
 * 
 */
class XTREMEMONOPOLY_API UIText:public UIElement
{
public:

	FString text;

	UIText();
	UIText(FVector2D position, FVector2D size, FColor col, FString msg, AXtremeMonopolyHUD *Screen);
	~UIText();

	virtual void OnEnter();
	virtual void Update();
	virtual void OnExit();
};

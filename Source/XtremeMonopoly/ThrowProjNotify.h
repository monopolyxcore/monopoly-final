// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimNotifies/AnimNotifyState.h"

#include "ThrowProjNotify.generated.h"


/**
 *
 */
UCLASS()
class XTREMEMONOPOLY_API UThrowProjNotify : public UAnimNotifyState
{
	GENERATED_BODY()


public:
	virtual bool Received_NotifyBegin(
		USkeletalMeshComponent * MeshComp,
		UAnimSequenceBase * Animation,
		float TotalDuration
		) const override;
	virtual bool Received_NotifyTick(
		USkeletalMeshComponent * MeshComp,
		UAnimSequenceBase * Animation,
		float FrameDeltaTime
		) const override;
	virtual bool Received_NotifyEnd(
		USkeletalMeshComponent * MeshComp,
		UAnimSequenceBase * Animation
		) const override;
};

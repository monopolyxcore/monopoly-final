// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "UIText.h"

UIText::UIText() :UIElement(), text("")
{
}

UIText::UIText(FVector2D position, FVector2D size, FColor col, FString msg, AXtremeMonopolyHUD *Screen)
{
	centerpos = position;
	scl = size;
	color = col;
	text = msg;
	hud = Screen;
}

UIText::~UIText()
{
}

void UIText::OnEnter()
{

}

void UIText::Update()
{
	if (hud)
	{
		if (state == 1)
		{
			
			hud->DrawText(text, color, GetStartDrawPos().X, GetStartDrawPos().Y, hud->HUDFont);
			//hud->DrawText(text, color, centerpos.X, centerpos.Y, hud->HUDFont);
		}
	}
}

void UIText::OnExit()
{

}

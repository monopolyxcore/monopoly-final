// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "UIElement.h"

UIElement::UIElement() :StateMachine(),centerpos(FVector2D()), scl(FVector2D()), color(FColor::White), hud(NULL)
{
}

UIElement::UIElement(FVector2D position, FVector2D size, FColor col, AXtremeMonopolyHUD *Screen) : StateMachine()
{
	centerpos = position;
	scl = size; 
	color = col;
	hud = Screen;
}

UIElement::~UIElement()
{
}

void UIElement::OnEnter()
{

}

void UIElement::Update()
{
	if (hud)
	{
		if (state == 1)
		{
			hud->DrawRect(color, GetStartDrawPos().X, GetStartDrawPos().Y, scl.X, scl.Y);
		}
	}
}

void UIElement::OnExit()
{

}

FVector2D UIElement::GetStartDrawPos()
{
	return FVector2D(centerpos.X - scl.X / 2, centerpos.Y - scl.Y / 2);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "ConcretePickUp.h"



AConcretePickUp::AConcretePickUp(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	ConcreteLevel = 0.f;
}

void AConcretePickUp::OnPickedUp_Implementation()
{
	Super::OnPickedUp_Implementation();
	Destroy();
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UIElement.h"
class UIElement;
/**
 * 
 */
class XTREMEMONOPOLY_API UITexture:public UIElement
{
public:

	UTexture* texture;

	UITexture();
	UITexture(FVector2D position, FVector2D size, FColor col, UTexture* tex, AXtremeMonopolyHUD *Screen);
	~UITexture();

	virtual void OnEnter();
	virtual void Update();
	virtual void OnExit();
};

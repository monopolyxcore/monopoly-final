// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "ThrowProjNotify.h"




bool UThrowProjNotify::Received_NotifyBegin(
	USkeletalMeshComponent * MeshComp,
	UAnimSequenceBase * Animation,
	float TotalDuration
	) const {
	return true;
}
bool UThrowProjNotify::Received_NotifyTick(
	USkeletalMeshComponent * MeshComp,
	UAnimSequenceBase * Animation,
	float FrameDeltaTime
	) const {
	return true;
}
bool UThrowProjNotify::Received_NotifyEnd(
	USkeletalMeshComponent * MeshComp,
	UAnimSequenceBase * Animation
	) const {
	return true;
}
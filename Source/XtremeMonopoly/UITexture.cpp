// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "UITexture.h"

UITexture::UITexture() :UIElement(), texture(NULL)
{
}

UITexture::UITexture(FVector2D position, FVector2D size, FColor col, UTexture* tex, AXtremeMonopolyHUD *Screen)
{
	centerpos = position;
	scl = size;
	color = col;
	texture = tex;
	hud = Screen;
}

UITexture::~UITexture()
{
}

void UITexture::OnEnter()
{

}

void UITexture::Update()
{
	if (hud)
	{
		if (state == 1)
		{
			hud->DrawTexture(texture, GetStartDrawPos().X, GetStartDrawPos().Y, scl.X, scl.Y, 0, 0, 1, 1);
		}
	}
}

void UITexture::OnExit()
{

}


#pragma once

#include "GameFramework/PlayerController.h"
#include "XtremeMonopolyPlayerController.generated.h"

/**
 *
 */
UCLASS()
class XTREMEMONOPOLY_API AXtremeMonopolyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	UFUNCTION()
		void OnWinTrigger();
};

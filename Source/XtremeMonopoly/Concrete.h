// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MyPickUps.h"
#include "Concrete.generated.h"

UCLASS()

class XTREMEMONOPOLY_API AConcrete : public AActor
{
	GENERATED_UCLASS_BODY()
public:

	AMyPickUps* CreateConcrete;

};

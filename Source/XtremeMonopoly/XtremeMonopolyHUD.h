// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"


#include "XtremeMonopolyHUD.generated.h"

/**
 * 
 */
UCLASS()
class XTREMEMONOPOLY_API AXtremeMonopolyHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

		UPROPERTY()

		UFont* HUDFont;
	
		UTexture* Icon1;

		FVector2D ScreenDimensions;
		

	virtual void DrawHUD() OVERRIDE;
};
